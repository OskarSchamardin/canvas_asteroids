/* This file handles general helper functions,
 * such as helping with math or collision detection.
 */

function degToRad(deg) {
    return deg * (Math.PI / 180);
}

function radToDeg(rad) {
    return rad * (180 / Math.PI);
}

function randomBetween(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function linesIntersect(a,b,c,d,p,q,r,s) {
    /* returns true if the line from (a,b) -> (c,d) intersects with (p,q) -> (r,s) */
    /* copied off stackoverflow: https://stackoverflow.com/a/24392281 */
    let det, gamma, lambda;
    det = (c - a) * (s - q) - (r - p) * (d - b);
    if (det === 0) {
        return false;
    } else {
        lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
        gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
        return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
    }
};

function playTone(frequency, type, duration) {
    /* generates and plays a tone */

    let oscillator = audioContext.createOscillator();

    oscillator.type = type;
    oscillator.frequency.value = frequency;
    oscillator.connect(gainNode);
    gainNode.connect(audioContext.destination);

    gainNode.gain.value = 0.02; /* Set volume (be careful not to blast out your ears) */

    oscillator.start();

    setTimeout(function() {
        oscillator.stop();
    }, duration);
}

function incrementAsteroids(size, operator) {
    let big    = document.getElementById('bigAsteroidMenuCounter');
    let medium = document.getElementById('mediumAsteroidMenuCounter');
    let small  = document.getElementById('smallAsteroidMenuCounter');
    let random = document.getElementById('randomAsteroidMenuCounter');

    switch (size) {
        case 'big':
            big.innerHTML = (operator === '+') ? Number(big.innerHTML) + 1 : big.innerHTML - 1;
            break;
        case 'medium':
            medium.innerHTML = (operator === '+') ? Number(medium.innerHTML) + 1 : medium.innerHTML - 1;
            break;
        case 'small':
            small.innerHTML = (operator === '+') ? Number(small.innerHTML) + 1 : small.innerHTML - 1;
            break;
        case 'random':
            random.innerHTML = (operator === '+') ? Number(random.innerHTML) + 1 : random.innerHTML - 1;
            break;
    }
}
