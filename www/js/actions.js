/* This file handles the actions executed when a button is pressed,
 * such as player ship's throttle and rotation.
 */

function playerThrottleUp() {
    /* accelerate player if max speed is not yet reached */
    playerSpeed += (playerSpeed < maxSpeed) && 0.25;
    playerIsAccelerating = true;
}

function playerThrottleDown() {
    /* decelerate player if min speed is not yet reached */
    playerSpeed -= (playerSpeed > minSpeed) && 0.25;
}

function playerRotateClockwise() {
    /* rotates player's ship clockwise */
    playerRotationAngle = (playerRotationAngle > 360) ? 5 : playerRotationAngle + 5;
    playerVelocityAngle = (playerVelocityAngle > 360) ? 5 : playerVelocityAngle + 5;
}

function playerRotateCounterclockwise() {
    /* rotates player's ship counterclockwise */
    playerRotationAngle = (playerRotationAngle < 0) ? 355 : playerRotationAngle - 5;
    playerVelocityAngle = (playerVelocityAngle < 0) ? 355 : playerVelocityAngle - 5;
}

function showMenuModal(makeVisible = true) {
    /* toggle main menu visibility */

    clearInterval(gameplayLoop); // end game silently

    let modal = document.getElementById('menuModal');

    if(makeVisible) {
        modal.style.display = 'block';
    } else {
        modal.style.display = 'none';
    }

    document.getElementById('restartButton').style.display = 'none';
}

function handleTouchActions() {
    /* runs actions multiple times until touched button is released */
    if(playerThrottleDirection !== null) {
        switch (playerThrottleDirection) {
            case 'forward':
                playerThrottleUp();
                break;
            case 'backward':
                playerThrottleDown();
                break;
        }
    }
    if(playerRotationDirection !== null) {
        switch (playerRotationDirection) {
            case 'clockwise':
                playerRotateClockwise();
                break;
            case 'counterclockwise':
                playerRotateCounterclockwise();
                break;
        }
    }
    if(playerIsShooting !== null && playerIsShooting) {
        shootBullet();
    }
}
