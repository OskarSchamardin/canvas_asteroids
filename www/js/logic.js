/* This file handles game logic functions that use other functions.
 * These functions handle everything between per object collision to
 * game over conditions.
 */

function playerInvulnerabilityHandler(blinkDuration, invulnerabilityDuration) {
    /* handles player invulnerability and the associated visual effects */

    let player = objects[0];

    let intervalId = setInterval(function() {
        if(player.colorCurrent === player.colorInvulnerable) {
            player.colorCurrent = player.colorNormal;
        } else {
            player.colorCurrent = player.colorInvulnerable;
        }
    }, blinkDuration);

    setTimeout(function() {
        clearInterval(intervalId);
        player.isInvulnerable = false;
        player.colorCurrent = player.colorNormal;
    }, invulnerabilityDuration);
}

function handlePlayerCollision() {
    /* detects if the player collides with anything */

    if (objects[0].isInvulnerable) { return } // Skip collision checking if player is invulnerable

    objects.forEach(function(object, i) {
        if (object.type === 'player') { return };       // Skip detecting if player collides with player
        if (object.type === 'playerBullet') { return }; // Skip detecting if player collides with playerBullet

        let o = objects[i].lines; // 'o' for non player object
        let p = objects[0].lines; // 'p' for player

        for(let i = 0; i < p.length - 1; i++) {
            /* all lines in player's ship */

            for(let j = 0; j < o.length - 1; j++) {
                /* all lines in object */

                if(linesIntersect(
                    (p[i    ].a + p[i    ].aMod), (p[i    ].b + p[i    ].bMod), // start line 1 (player)
                    (p[i + 1].a + p[i + 1].aMod), (p[i + 1].b + p[i + 1].bMod), // end line   1 (player)
                    (o[j    ].a + o[j    ].aMod), (o[j    ].b + o[j    ].bMod), // start line 2 (other)
                    (o[j + 1].a + o[j + 1].aMod), (o[j + 1].b + o[j + 1].bMod), // end line   2 (other)
                )) {
                    console.log(`BOOM! The player collided with something!`);
                    objects[0].hitpoints--;
                    if(objects[0].hitpoints === -1) {
                        gameOverHandler();
                    }
                }
            }
        }
    });
}

function handlePlayerBulletCollision() {
    /* detects if a shot bullet collides with anything */

    objects.forEach(function(object, i) {
        if (object.type === 'player') { return };       // Skip detecting if player collides with playerBullet
        if (object.type === 'playerBullet') { return }; // Skip detecting if playerBullet collides with playerBullet

        let o = objects[i].lines;       // 'o' for non playerBullet object
        let otherObject = objects[i]    // used for potential object deletion

        objects.forEach(function(bullet) {
            if(bullet.type === 'playerBullet') {
            /* all shot bullets */

                let p = bullet.lines; // 'p' for playerBullet

                for(let i = 0; i < p.length - 1; i++) {
                    /* all lines in bullet */

                    for(let j = 0; j < o.length - 1; j++) {
                        /* all lines in object */

                        if(linesIntersect(
                            (p[i    ].a + p[i    ].aMod), (p[i    ].b + p[i    ].bMod), // start line 1 (bullet)
                            (p[i + 1].a + p[i + 1].aMod), (p[i + 1].b + p[i + 1].bMod), // end line   1 (bullet)
                            (o[j    ].a + o[j    ].aMod), (o[j    ].b + o[j    ].bMod), // start line 2 (other)
                            (o[j + 1].a + o[j + 1].aMod), (o[j + 1].b + o[j + 1].bMod), // end line   2 (other)
                        )) {
                            delete objects[bullet.id];  // delete bullet
                            let points;                 // amount of points to reward player with

                            switch(object.size) {
                                case 'big':
                                    addAsteroid('medium', 5, 2, otherObject.x, otherObject.y);
                                    playTone(150, 'square', 75);
                                    points = 300;
                                    break;
                                case 'medium':
                                    addAsteroid('small', 5, 2, otherObject.x, otherObject.y);
                                    playTone(200, 'square', 75);
                                    points = 150;
                                    break;
                                case 'small':
                                    playTone(250, 'square', 75);
                                    points = 50;
                                    break;
                                case 'default':
                                    playTone(200, 'square', 75);
                                    points = 100;
                                    break;
                            }

                            delete objects[objects.indexOf(otherObject)];   // delete shot object
                            score += points;       // add sense of pride and accomplishment

                            let asteroidsRemaning = 0;

                            objects.forEach(function(object) {
                                if(object.type === 'asteroid') { asteroidsRemaning++ } // count remaning asteroids
                            });

                            if(asteroidsRemaning === 0) {
                                /* end game with victory if no asteroids are left */
                                drawNextFrame();
                                gameOverHandler(true);
                            }
                        }
                    }
                }
            }
        });
    });
}

function gameOverHandler(victory = false) {
    /* stops the game loop and displays game over text */

    clearInterval(gameplayLoop);

    let gameOverText;                           // big text indicating win/loss
    let extraText;                              // extra caption
    let restartText = '(Hit "r" to restart.)';  // instruct player to play another round

    gameOverText = (victory) ? 'You win!' : 'Game over!';

    if(victory) {
        playMedley(victoryMedley);
        extraText = [
            "You really did it!",
            "Told you it would work out this time.",
            `You managed to earn ${score} points!`,
            "Pew! Pew! Boom! Boom! Victory text!",
            `Your score: ${score}.`,
            "Looks like your luck got better.",
        ];
    } else {
        playMedley(gameOverMedley);
        extraText = [
            "Talk about janky collision detection, eh?",
            "It was going so well! :(",
            "I also think the game was a little too mean there.",
            "Better luck next time?",
            "It'll work out next time.",
            "What just happened?!",
        ];
    }


    ctx.font = 'bold 100px Arial';
    ctx.fillStyle= '#FFFFFF';
    ctx.textBaseline = 'middle';
    ctx.textAlign = 'center';

    ctx.fillText(gameOverText, window.innerWidth / 2, window.innerHeight / 2);

    ctx.font = 'bold 20px Arial';
    ctx.fillText(extraText[randomBetween(0, extraText.length - 1)], window.innerWidth / 2, window.innerHeight / 2 + 50);
    ctx.fillText(restartText, window.innerWidth / 2, window.innerHeight / 2 + 75);

    document.getElementById('restartButton').style.display = 'block';
}

function playMedley(tones) {
    /* plays a sequence according to its instructions */
    tones.forEach(function(t) {
        setTimeout(function() { playTone(t.frequency, t.type, t.duration) }, t.startTime);
    });
}
