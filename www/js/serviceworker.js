/* This file handles the configuration of a service worker that 
 * will manage the working of the application.
 * Copied off geeksforgeeks: https://www.geeksforgeeks.org/making-a-simple-pwa-under-5-minutes/
 */

let staticCacheName = "pwa";

self.addEventListener("install", function (e) {
    e.waitUntil(
        caches.open(staticCacheName).then(function (cache) {
            return cache.addAll(["/"]);
        })
    );
});

self.addEventListener("fetch", function (event) {
    console.log(event.request.url);

    event.respondWith(
        caches.match(event.request).then(function (response) {
            return response || fetch(event.request);
        })
    );
});
