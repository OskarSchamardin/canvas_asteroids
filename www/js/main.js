/* This is the main file for game logic.
 * This calles functions from other files.
 */

window.addEventListener('load', function() {
    /* load game after canvas has loaded */

    let gamescreen = document.getElementById('gamescreen'); // canvas containing play area
    ctx = gamescreen.getContext('2d');                      // canvas context

    gamescreen.width = window.innerWidth;
    gamescreen.height = window.innerHeight;
});

function newgame() {
    /* starts a new game */

    /* (re)sets gamescreen size */
    gamescreen.width = window.innerWidth;
    gamescreen.height = window.innerHeight;

    /* sorry for bad and repetitive code */
    let bigAsteroids         = Number(document.getElementById('bigAsteroidMenuCounter').innerHTML);
    let mediumAsteroids      = Number(document.getElementById('mediumAsteroidMenuCounter').innerHTML);
    let smallAsteroids       = Number(document.getElementById('smallAsteroidMenuCounter').innerHTML);
    let randomAsteroids      = Number(document.getElementById('randomAsteroidMenuCounter').innerHTML);
    let bigAsteroidsCount    = (bigAsteroids)    ? bigAsteroids    : 0;
    let meniumAsteroidsCount = (mediumAsteroids) ? mediumAsteroids : 0;
    let smallAsteroidsCount  = (smallAsteroids)  ? smallAsteroids  : 0;
    let randomAsteroidsCount = (randomAsteroids) ? randomAsteroids : 0;

    showMenuModal(false);   // hide main menu

    /* clean up variables after last game */
    bulletCooldownInEffect = false;
    objects = [];
    playerIsAccelerating = false;
    playerRotationAngle = 0;
    playerSpeed = 0.00;
    playerVelocityAngle = 270;
    score = 0;

    /* add player's ship (should be first in objects array) */
    addPlayerShip(3);

    /* add asteroid(size, lineWidth, amount) */
    addAsteroid('big',    5, bigAsteroidsCount);
    addAsteroid('medium', 5, meniumAsteroidsCount);
    addAsteroid('small',  5, smallAsteroidsCount);
    addAsteroid('random', 5, randomAsteroidsCount);

    /* starting invulnerability for player */
    playerInvulnerabilityHandler(200, 5000);

    /* game loop */
    gameplayLoop = setInterval(function() {
        drawNextFrame();                    // handles drawing and moving of elements
        handlePlayerCollision();            // handles player collision and triggers game over when player loses all hitpoints
        handlePlayerBulletCollision();      // handles what to do when the player's bullets hit something
        playerIsAccelerating = false;       // stop drawing player ship's exhaust
    }, 16); // ~60 fps
}
