/* This file contains global variables and constants. */

const maxSpeed = 10;                    // max usable speed
const minSpeed = 0;                     // min usable speed
let bulletCooldownInEffect;             // score counter, basic arcade stuff
let ctx;                                // canvas context
let gameplayLoop;                       // id given by the 'setInterval' function for the game loop
let objects = [];                       // contains all drawable objects (player's ship should be first)

let playerIsAccelerating;               // used to determine if player ship's exhaust should be drawn
let playerRotationAngle;                // angle that the ship faces
let playerSpeed;                        // speed should be between 0.00 and 4.00
let playerVelocityAngle;                // angle that the ship moves in
let playerThrottleDirection = null;     // direction in which player is moving, helper for touch input
let playerRotationDirection = null;     // direction in which player is rotating, helper for touch input
let playerIsShooting = null;            // helper for touch input

let score;                              // score counter, basic arcade stuff

const audioContext = new(window.AudioContext || window.webkitAudioContext)(); // context for sound generation
const gainNode = audioContext.createGain();                                   // volume control
