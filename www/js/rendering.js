/* This file handles the drawing functions,
 * such as adding drawable items and drawing each frame.
 */

function drawNextFrame() {
    /* do all nessecary actions and draw next frame */

    handleTouchActions();   // runs any actions called by touched and held down buttons

    /* draw all objects */
    ctx.clearRect(0,0, gamescreen.width, gamescreen.height);
    objects.forEach((object) => {
        /* move object to other side of screen when going off the edge */
        let multiplier = (object.type === 'player') ? 0 : 200; // modifier for nonplayer objects to go more off screen
        /* Going west */
        if(object.x < 0 - multiplier) {
            object.x = gamescreen.width + multiplier;
        /* Going north */
        } else if(object.y < 0 - multiplier) {
            object.y = gamescreen.height + multiplier;
        /* Going east */
        } else if(object.x > gamescreen.width + multiplier) {
            object.x = 0 - multiplier;
        /* Going south */
        } else if(object.y > gamescreen.height + multiplier) {
            object.y = 0 - multiplier;
        }

        drawObject(object);
    });

    if(playerIsAccelerating) {
        /* draw cool flames coming out of player's ship */
        drawObject(objects[0], 'exhaustLines', 'colorExhaust');
    }

    /* counters */
    ctx.font = 'bold 20px Arial';
    ctx.fillStyle= '#FFFFFF';

    ctx.fillText(`Hitpoints: ${objects[0].hitpoints}`, 0, 20);
    ctx.fillText(`Score: ${score}`, 0, 40);
}

function drawObject(object, instructionSet = 'lines', color = 'colorCurrent') {
    /* determine what object to draw and draw it */

    /* setup drawing */
    ctx.beginPath();
    ctx.strokeStyle = object[color];
    ctx.lineWidth = object.lineWidth;

    /* object rotation */
    ctx.translate(object.x, object.y)
    if(object.type === 'player') {
        ctx.rotate(degToRad(playerRotationAngle));
    } else {
        ctx.rotate(degToRad(object.rotationAngle));
    }
    ctx.translate(-1 * object.x, -1 * object.y)

    object[instructionSet].forEach(function(line, i) {
        /* draw object with its instructions */
        switch (i) {
            case 0:
                /* move in first iteration */
                ctx.moveTo(line.a + line.aMod, line.b + line.bMod);
                break;
            case (object.lines.length - 1):
                /* draw with lineWidth in last iteration */
                ctx.lineTo(line.a + ctx.lineWidth / 2 + line.aMod, line.b + line.bMod);
                break;
            default:
                ctx.lineTo(line.a + line.aMod, line.b + line.bMod);
                break;
        }
    });

    switch (object.type) {
        /* handle object movements */
        case 'asteroid':
            /* move asteroid */
            object.x += Math.cos(degToRad(object.velocityAngle)) * object.speed;
            object.y += Math.sin(degToRad(object.velocityAngle)) * object.speed;
            break;
        case 'playerBullet':
            /* move bullet shot by player */
            object.x += Math.cos(degToRad(object.velocityAngle)) * object.speed;
            object.y += Math.sin(degToRad(object.velocityAngle)) * object.speed;
            break;
        case 'player':
            /* move player */
            if(instructionSet === 'lines') {
                object.x += Math.cos(degToRad(playerVelocityAngle)) * playerSpeed;
                object.y += Math.sin(degToRad(playerVelocityAngle)) * playerSpeed;
            }
            break;
    }

    if(instructionSet !== 'exhaustLines') {
        object[instructionSet].forEach(function(line) {
            /* update line coordinates according to movement */
            line.a = object.x;
            line.b = object.y;
        });
        /* handle exhaust instructions via player and not independently (this prevents visual rotation bugs) */
        if(object.type === 'player') {
            object.exhaustLines.forEach(function(line) {
                line.a = object.x;
                line.b = object.y;
            });
        }
    }

    ctx.setTransform(1, 0, 0, 1, 0, 0);     // reset canvas rotation
    ctx.stroke();                           // draw outline
    ctx.closePath();
}

function addAsteroid(size, lineWidth, amount,
    x = Math.floor(Math.random() *gamescreen.width),
    y = Math.floor(Math.random() *gamescreen.height),) {

    for(let i = 0; i < amount; i++) {
        switch (size) {
            case 'big':
                addBigAsteroid(lineWidth, x, y);
                break;
            case 'medium':
                addMediumAsteroid(lineWidth, x, y);
                break;
            case 'small':
                addSmallAsteroid(lineWidth, x, y);
                break;
            case 'random':
                addRandomAsteroid(lineWidth, x, y);
                break;
        }
    }
}

function addBigAsteroid(lineWidth, x, y) {
    /* adds a big asteroid to the list of objects */
    objects.push({
        colorCurrent: '#FF00BB',
        lines: [
            // outline drawing instructions
            { a: this.x, b: this.y, aMod: 0,    bMod: -100 },
            { a: this.x, b: this.y, aMod: 100,  bMod: -40 },
            { a: this.x, b: this.y, aMod: 30,   bMod: 30 },
            { a: this.x, b: this.y, aMod: -50,  bMod: 50 },
            { a: this.x, b: this.y, aMod: -100, bMod: -75 },
            { a: this.x, b: this.y, aMod: -50,   bMod: -100 },
            { a: this.x, b: this.y, aMod: 0,    bMod: -100 },
        ],
        lineWidth: lineWidth,
        rotationAngle: Math.floor(Math.random() * 360),
        size: 'big',
        speed: (Math.floor(Math.random() * (maxSpeed - 1)) + 1) / 2,
        type: "asteroid",
        velocityAngle: Math.floor(Math.random() * 360),
        x: x,
        y: y,
    });
}

function addMediumAsteroid(lineWidth, x, y) {
    /* adds a medium asteroid to the list of objects */
    objects.push({
        colorCurrent: '#FF00BB',
        lines: [
            // outline drawing instructions
            { a: this.x, b: this.y, aMod: 0,    bMod: -10 },
            { a: this.x, b: this.y, aMod: 10,  bMod: -40 },
            { a: this.x, b: this.y, aMod: 30,   bMod: 30 },
            { a: this.x, b: this.y, aMod: -50,  bMod: 50 },
            { a: this.x, b: this.y, aMod: -60, bMod: 0 },
            { a: this.x, b: this.y, aMod: -120,   bMod: -75 },
            { a: this.x, b: this.y, aMod: -50,    bMod: -50 },
            { a: this.x, b: this.y, aMod: 0,    bMod: -10 },
        ],
        lineWidth: lineWidth,
        rotationAngle: Math.floor(Math.random() * 360),
        size: 'medium',
        speed: (Math.floor(Math.random() * (maxSpeed - 1)) + 1) / 2,
        type: "asteroid",
        velocityAngle: Math.floor(Math.random() * 360),
        x: x,
        y: y,
    });
}

function addSmallAsteroid(lineWidth, x, y) {
    /* adds a small asteroid to the list of objects */
    objects.push({
        colorCurrent: '#FF00BB',
        lines: [
            // outline drawing instructions
            { a: this.x, b: this.y, aMod: 0,    bMod: -30 },
            { a: this.x, b: this.y, aMod: 40,  bMod: -20 },
            { a: this.x, b: this.y, aMod: 30,   bMod: 30 },
            { a: this.x, b: this.y, aMod: 0,  bMod: 50 },
            { a: this.x, b: this.y, aMod: 0,  bMod: -30 },
        ],
        lineWidth: lineWidth,
        rotationAngle: Math.floor(Math.random() * 360),
        size: 'small',
        speed: (Math.floor(Math.random() * (maxSpeed - 1)) + 1) / 2,
        type: "asteroid",
        velocityAngle: Math.floor(Math.random() * 360),
        x: x,
        y: y,
    });
}

function addRandomAsteroid(lineWidth, x, y) {
    /* adds a randomly generated asteroid to the list of objects */

    let lines = [];

    /* randomize drawing instructions. !!! MIN MUST BE 4 OR MORE !!! */
    let asteroidComplexity = randomBetween(4, 10);
    for(let i = 0; i < asteroidComplexity; i++) {
        if(i === 0) {
            /* random starting point */
            lines.push({ a: x, b: y, aMod: randomBetween(20, 100), bMod: randomBetween(20, 100)});
        } else if(i === asteroidComplexity - 1) {
            /* end where asteroid started */
            lines.push(lines[0]);
        } else {
            /* randomize asteroid size */
            lines.push({ a: x, b: y, aMod: randomBetween(-100, 100), bMod: randomBetween(-100, 100)});
        }
    }
    objects.push({
        colorCurrent: '#FF00BB',
        lines: lines,
        lineWidth: lineWidth,
        rotationAngle: Math.floor(Math.random() * 360),
        speed: (Math.floor(Math.random() * (maxSpeed - 1)) + 1) / 2,
        size: 'small',
        type: "asteroid",
        velocityAngle: Math.floor(Math.random() * 360),
        x: x,
        y: y,
    });
}

function addPlayerShip(lineWidth) {
    /* add player's ship */
    objects[0] = {
        colorCurrent:      '#00AFFF',   // current player colour
        colorNormal:       '#00AFFF',   // colour to use normally
        colorExhaust:      '#FF4400',   // colour of flame during acceleration
        colorInvulnerable: '#CCCCCC',   // colour to flash to indicate player invulnerability
        isInvulnerable: true,           // 5 second invulnerability
        lineWidth: lineWidth,
        hitpoints: 100,                 // health to make the game a bit more fair
        type: "player",
        x: gamescreen.width / 2,        // center horizontally
        y: gamescreen.height / 2,       // center vertically
        lines: [
            // outline drawing instructions
            { a: this.x / 2, b: this.y, aMod: 0,   bMod: -15 },
            { a: this.x / 2, b: this.y, aMod: 15,  bMod: 25 },
            { a: this.x / 2, b: this.y, aMod: -15, bMod: 25 },
            { a: this.x / 2, b: this.y, aMod: 0,   bMod: -15 },
        ],
        exhaustLines: [
            // ship exhaust drawing instructions
            { a: this.x / 2, b: this.y, aMod: 10,  bMod: 45 },
            { a: this.x / 2, b: this.y, aMod: 15,  bMod: 70 },
            { a: this.x / 2, b: this.y, aMod: 10,  bMod: 60 },
            { a: this.x / 2, b: this.y, aMod: 0,   bMod: 90 },
            { a: this.x / 2, b: this.y, aMod: -10, bMod: 60 },
            { a: this.x / 2, b: this.y, aMod: -15, bMod: 70 },
            { a: this.x / 2, b: this.y, aMod: -10, bMod: 45 },
            { a: this.x / 2, b: this.y, aMod: 10,  bMod: 45 },
        ],
    }
}

function shootBullet(lineWidth) {
    /* adds a bullet in front of the player */

    if(bulletCooldownInEffect) { return; } // stops bulletspam

    let player = objects[0];
    let rotationAngle = playerRotationAngle;
    let velocityAngle = playerVelocityAngle;
    let id = objects.length;

    objects.push({
        id: id,
        colorCurrent: '#00AFFF',
        lines: [
            // outline drawing instructions
            { a: player.x, b: player.y, aMod: 0,  bMod: -20 },
            { a: player.x, b: player.y, aMod: 0,  bMod: -70 },
        ],
        lineWidth: lineWidth,
        rotationAngle: rotationAngle,
        speed: 20,
        type: "playerBullet",
        velocityAngle: velocityAngle,
        x: player.x,
        y: player.y,
    });

    playTone(450, 'triangle', 100); // add sound for immersion (or annoyance). Pew, pew!

    setTimeout(function() {
        /* delete bullet if it has not hit anything */
        if(objects[id] !== undefined) {
            delete objects[id];
        }
    }, 2500);

    setTimeout(function() {
        /* stop bullet shooting cooldown */
        bulletCooldownInEffect = false;
    }, 350);

    bulletCooldownInEffect = true; // stop bullet spam by imposing a cooldown
}
