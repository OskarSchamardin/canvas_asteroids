/* This file contains objects with instructions to play sound sequences,
 * such as explosions and game over medleies.
 */

const gameOverMedley = [
    {frequency: 300, type: 'square', duration: 200, startTime: 100},
    {frequency: 250, type: 'square', duration: 200, startTime: 300},
    {frequency: 200, type: 'square', duration: 200, startTime: 500},
    {frequency: 150, type: 'square', duration: 200, startTime: 700},
    {frequency: 100, type: 'square', duration: 400, startTime: 900},
];
const victoryMedley = [
    {frequency: 100, type: 'square', duration: 200, startTime: 100},
    {frequency: 150, type: 'square', duration: 200, startTime: 300},
    {frequency: 200, type: 'square', duration: 200, startTime: 500},
    {frequency: 250, type: 'square', duration: 200, startTime: 700},
    {frequency: 300, type: 'square', duration: 400, startTime: 900},
];
