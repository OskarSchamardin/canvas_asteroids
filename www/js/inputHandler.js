/* This file handles input related behaviour,
 * such as control layouts and event handlers.
 */

KeyboardController({
    /* keyboard input handling without waiting for key repeat */
    /* shooting */
    32: () => { shootBullet(3); },                          // Spacebar
    13: () => { shootBullet(3); },                          // Enter

    /* movement (Arrowkeys) */
    37: () => { playerRotateCounterclockwise(); },          // ArrowLeft
    38: () => { playerThrottleUp(); },                      // ArrowUp
    39: () => { playerRotateClockwise(); },                 // ArrowRight
    40: () => { playerThrottleDown(); },                    // ArrowDown

    /* movement (WASD) */
    65: () => { playerRotateCounterclockwise(); },          // A
    87: () => { playerThrottleUp(); },                      // W
    68: () => { playerRotateClockwise(); },                 // D
    83: () => { playerThrottleDown(); },                    // S

    /* movement (VIM) */
    72: () => { playerRotateCounterclockwise(); },          // H
    75: () => { playerThrottleUp(); },                      // K
    76: () => { playerRotateClockwise(); },                 // L
    74: () => { playerThrottleDown(); },                    // J

    /* restart game */
    82: () => { showMenuModal(true); },                     // R
}, 16); // ~60 fps

function KeyboardController(keys, repeat) {
    // Keyboard input with customisable repeat (set to 0 for no key repeat)
    /* Copied off stackoverflow: https://stackoverflow.com/a/3691661 */

    // Lookup of key codes to timer ID, or null for no repeat
    var timers= {};

    document.onkeydown= function(event) {
        // When key is pressed and we don't already think it's pressed, call the
        // key action callback and set a timer to generate another one after a delay
        var key= (event || window.event).keyCode;
        if (!(key in keys))
            return true;
        if (!(key in timers)) {
            timers[key]= null;
            keys[key]();
            if (repeat!==0)
                timers[key]= setInterval(keys[key], repeat);
        }
        return false;
    };

    document.onkeyup= function(event) {
        // Cancel timeout and mark key as released on keyup
        var key= (event || window.event).keyCode;
        if (key in timers) {
            if (timers[key]!==null)
                clearInterval(timers[key]);
            delete timers[key];
        }
    };

    window.onblur= function() {
        // When window is unfocused we may not get key events. To prevent this
        // causing a key to 'get stuck down', cancel all held keys
        for (key in timers)
            if (timers[key]!==null)
                clearInterval(timers[key]);
        timers= {};
    };
};

document.addEventListener('DOMContentLoaded', function() {
    /* sets up canvases for touch controls */
    setupTouchHandlers('gamescreen', 'shoot');
    setupTouchHandlers('touchThrottleUp', 'up');
    setupTouchHandlers('touchThrottleDown', 'down');
    setupTouchHandlers('touchRotateClockwise', 'right');
    setupTouchHandlers('touchRotateCounterclockwise', 'left');
});

function setupTouchHandlers(id, action) {
    /* set up event handlers for elements */
    let el = document.getElementById(id);

    /* TODO: refactor */
    switch (action) {
        case 'shoot':
            el.ontouchstart = function(e) {
                e.preventDefault();
                playerIsShooting = true;
            };
            el.ontouchend = function(e) {
                e.preventDefault();
                playerIsShooting = null;
            };
            break;
        case 'up':
            el.ontouchstart = function(e) {
                e.preventDefault();
                playerThrottleDirection = 'forward';
            };
            el.ontouchend = function(e) {
                e.preventDefault();
                playerThrottleDirection = null;
            };
            break;
        case 'down':
            el.ontouchstart = function(e) {
                e.preventDefault();
                playerThrottleDirection = 'backward';
            };
            el.ontouchend = function(e) {
                e.preventDefault();
                playerThrottleDirection = null;
            };
            break;
        case 'right':
            el.ontouchstart = function(e) {
                e.preventDefault();
                playerRotationDirection = 'clockwise';
            };
            el.ontouchend = function(e) {
                e.preventDefault();
                playerRotationDirection = null;
            };
            break;
        case 'left':
            el.ontouchstart = function(e) {
                e.preventDefault();
                playerRotationDirection = 'counterclockwise';
            };
            el.ontouchend = function(e) {
                e.preventDefault();
                playerRotationDirection = null;
            };
            break;
    }
}
