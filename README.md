# README

This is a game inspired by asteroids.

## Live demo using gitlab pages

[Link to live demo](https://oskarschamardin.gitlab.io/canvas_asteroids)

## Electron using cordova

```bash
git clone https://gitlab.com/OskarSchamardin/canvas_asteroids
cd canvas_asteroids
npm i               # install node modules
npm run platforms   # run a script that adds electron to cordova (may need
                    # cordova installed globally)
npm run electron    # run a script that runs electron with cordova (may need
                    # cordova installed globally)

# this may or may not work on windows, since the 'www' folder located in the
# 'cordova' folder is a symbolic link to the 'www' folder in the project root.
# If it doesn't work, then you need to remove the link in the 'cordova' folder
# and copy the 'www' folder located in the project root into the 'cordova'
# folder and run both npm commands again.
```

## Android using cordova

TBD

[Android without installing android studio](https://www.andreszsogon.com/setting-up-your-environment-for-apache-cordova-apps/)
